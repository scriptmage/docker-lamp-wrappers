# Wrappers for the docker-lamps image

These shell scripts will help you to use [mattrayner/lamp:latest-1804](https://hub.docker.com/r/mattrayner/lamp/) docker image a bit simpler.

## Pre-Condition

You should have only [Docker CE](https://docs.docker.com/install/linux/docker-ce/debian/)

## How to use them

1. You can check the launched server on [http://localhost](http://localhost)
2. You can edit the database on [http://localhost/phpmyadmin](http://localhost/phpmyadmin) page
    - username: **admin**
    - password: **admin**
3. You can connect to the mysql database on port 3306 with an external application as well

### create

The script will create the docker container with your specified name and will create a database with the same name with *ds_* prefix. You can use only alphabets and numerics in name of container.

```
sudo ./create your_project_name
```

### start

```
sudo ./start your_project_name
```

### stop

```
sudo ./stop
```

